import Vue from 'vue'
import Icon from '../global/Icon'
import Button from '../global/Button'
import Search from '../global/Search'
import Autocomplete from '../global/Autocomplete'
import Sidebar from '../global/Sidebar'
import List from '../global/List'
import ListItem from '../global/ListItem'
import ListItemAvatar from '../global/ListItemAvatar'
import ListItemTitle from '../global/ListItemTitle'
import Dialog from '../global/Dialog'
import Card from '../global/Card'
import CardTitle from '../global/CardTitle'
import CardContent from '../global/CardContent'
import CardButtons from '../global/CardButtons'
import Table from '../global/Table'
import FotoKiosk from '../global/FotoKiosk'
import Datatable from '../global/Datatable'
import Checkbox from '../global/Checkbox'
import Tooltip from '../global/Tooltip'

Vue.component('t-icon', Icon)
Vue.component('t-btn', Button)
Vue.component('t-search', Search)
Vue.component('t-autocomplete', Autocomplete)
Vue.component('t-sidebar', Sidebar)
Vue.component('t-list', List)
Vue.component('t-list-item', ListItem)
Vue.component('t-list-item-avatar', ListItemAvatar)
Vue.component('t-list-item-title', ListItemTitle)
Vue.component('t-dialog', Dialog)
Vue.component('t-card', Card)
Vue.component('t-card-title', CardTitle)
Vue.component('t-card-content', CardContent)
Vue.component('t-card-buttons', CardButtons)
Vue.component('t-table', Table)
Vue.component('t-foto-kiosk', FotoKiosk)
Vue.component('t-datatable', Datatable)
Vue.component('t-checkbox', Checkbox)
Vue.component('t-tooltip', Tooltip)

Vue.directive('late-focus', {
    inserted: (el, binding, vnode) => {
        setTimeout(() => {
            if (vnode.componentInstance && vnode.componentInstance.focus) {
                vnode.componentInstance.focus()
            }
            else if (el && el.focus) {
                el.firstChild[1].focus()
            }
            else console.log('focus error')
        }, 100)
    }
})