/*export default function ({ isHMR, app, store, route, params, req, error, redirect }) {
    if (isHMR) { // ignore if called from hot module replacement
      return;
    }
  
    if (req) {
      if (route.name) {
  
        // check if the locale cookie is set
        if (req.headers.cookie) {
          const cookies = req.headers.cookie.split('; ').map(stringCookie => stringCookie.split('='));
          const cookie = cookies.find((cookie) => cookie[0] === 'auth')
          
          if (!cookie) {
            console.log('need to authenticate')
            redirect(`https://auth.trikthom.com/?url=${req.url}`)
            } else if (cookie[1]) {
                console.log('you are authenticated')
                store.commit('auth/setAuthenticated', !!cookie[1])
                // redirect back
            }
        } else {
            console.log('need to authenticate')
            redirect(`https://auth.trikthom.com/?url=https://lego.trikthom.com`)
        }
      }
    }
  }

*/
import axios from 'axios'

export default function ({ redirect, store, route, env }) {
    const cookies = document.cookie.split('; ').map((stringCookie) => stringCookie.split('='))
    const cookie = cookies.find((cookie) => cookie[0] === '_trikthom_auth')

    if (!cookie) {
        console.log('need to authenticate')
        console.log(window.location.toString())

        if (env.NODE_ENV === 'dev') window.location.href = 'http://localhost:8080/?url=http://localhost:9000'
        else window.location.href = `https://auth.trikthom.com/?url=${window.location.href}`
    } else if (cookie[1]) {
        console.log('you are authenticated')
        store.commit('auth/setAuthenticated', !!cookie[1])
        //axios.defaults.headers.common['Authorization'] = 'Bearer ' + cookie[1];
        // redirect back
    }
}
