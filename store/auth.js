export const state = () => ({

    isAuthenticated: false,

})

export const getters = {

    isAuthenticated(state) {
        return state.isAuthenticated
    },

}

export const mutations = {

    setAuthenticated(state, value) {
        state.isAuthenticated = value
    },

}

export const actions = {

    async logout({commit}) {
        const cookie = '_trikthom_auth=;Max-Age=0;'
        if (process.env.NODE_ENV === 'production') {
            document.cookie = `${cookie}Domain=.trikthom.com;`
        } else if (process.env.NODE_ENV === 'dev') {
            document.cookie = `${cookie}Domain=.localhost;`
        }
        commit('setAuthenticated', false)
        window.location.href = '/'
    },

}
