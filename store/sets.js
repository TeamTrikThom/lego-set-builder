import axios from 'axios'

export const state = () => ({
  sets: [],
})

export const getters = {

  LIST(state) {
    return state.sets
  },

  BY_NUM: (state) => (set_num) => {
    return state.sets.find(set => set.set_num == set_num)
  }

}

export const mutations = {

  SET_SETS(state, sets) {

    state.sets = sets.map(set => {
      set.parts_needed = [] //TODO backend
      return set
    })
  },

  SET_PARTS_FOR_SET(state, {parts, set_num}) {
    console.log(parts)
    state.sets.find(set => set.set_num == set_num).parts_needed = parts
  }

}

export const actions = {

  async GET_NEEDED_PARTS(context, set_num) {
    try {
      if(set_num === 'all') {

        await context.dispatch('partsNeeded/REFRESH', {}, {root:true})
        
      } else await axios.get('https://api.trikthom.com/lego-set-builder/legacy/sets/' + set_num + '/partsneeded', { withCredentials: true }).then(({data}) => {
        console.log(data)
        context.commit('SET_PARTS_FOR_SET', {
          parts: data.results, set_num
        })
      })
      
    } catch (error) {
      console.log(error)
    }
    
  },

  async ADD_PART_TO_SET(context, {set_nums, element_id, quantity}) {
    return axios.post('https://rest.trikthom.com/lego/v2/parts', {
      part: {
        element_id: element_id,
        quantity: quantity
      },
      set_nums: set_nums, // [set] or all
    })
  },

  async REFRESH(context) {
    return axios.get('https://rest.trikthom.com/lego/v2/sets').then(({data}) => {
      context.commit('SET_SETS', data.results)
    })
  },

  async INITIALIZE(context) {
    return axios.get('https://rest.trikthom.com/lego/v2/sets').then(({data}) => {
      context.commit('SET_SETS', data.results)
    })
  }

}