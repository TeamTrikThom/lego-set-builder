import axios from 'axios'

export const state = () => ({
  parts: [],
})

export const getters = {

  LIST(state) {
    return state.parts
  },
}

export const mutations = {

    SET_PARTS(state, parts) {
        state.parts = parts
    },

}

export const actions = {

  async REFRESH(context) {
    return axios.get('https://api.trikthom.com/lego-set-builder/legacy/partsneeded', { withCredentials: true }).then(({data}) => {
      context.commit('SET_PARTS', data.results)
    })
  },
  
  async INITIALIZE(context) {
    return axios.get('https://api.trikthom.com/lego-set-builder/legacy/partsneeded', { withCredentials: true }).then(({data}) => {
      context.commit('SET_PARTS', data.results)
    })
  }

}