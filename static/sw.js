importScripts('/_nuxt/workbox.4c4f5ca6.js')

workbox.precaching.precacheAndRoute([
  {
    "url": "/_nuxt/1516c7de5e85b0e2ef04.js",
    "revision": "623e6b60f4a1403019a406786440c747"
  },
  {
    "url": "/_nuxt/196d78f0f1f59ec6aec1.js",
    "revision": "49797a7756813bdbe859d2c0484988eb"
  },
  {
    "url": "/_nuxt/2a35a05b1966bed2bedc.js",
    "revision": "6b998c74fe5d2881ab846fe00ebb4b45"
  },
  {
    "url": "/_nuxt/4bb4f537f58beef55491.js",
    "revision": "3d2d9ecb2e41e07f3f0dbf776a1c5a6c"
  },
  {
    "url": "/_nuxt/57f8d7d3e022f865c3c2.js",
    "revision": "98e14321bbbdad84ebde3e0c3ea81903"
  },
  {
    "url": "/_nuxt/ab72fd44677e216345b7.js",
    "revision": "64fc4b73306198bbce956f9f2cbcdce8"
  },
  {
    "url": "/_nuxt/b1d1742c940b79bb41cd.js",
    "revision": "dc6fdb0127a0ffde36804926a055da24"
  },
  {
    "url": "/_nuxt/c58603bd43b58d16e7a0.js",
    "revision": "8b1c2018f2980559460850ce1e6d04b0"
  },
  {
    "url": "/_nuxt/c82ba6b4c8e2309ff483.js",
    "revision": "0b81f7c24ec230b43233a0b2ac7103bf"
  }
], {
  "cacheId": "lego-set-builder",
  "directoryIndex": "/",
  "cleanUrls": false
})

workbox.clientsClaim()
workbox.skipWaiting()

workbox.routing.registerRoute(new RegExp('/_nuxt/.*'), workbox.strategies.cacheFirst({}), 'GET')

workbox.routing.registerRoute(new RegExp('/.*'), workbox.strategies.networkFirst({}), 'GET')
